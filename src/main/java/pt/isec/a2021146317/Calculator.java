package pt.isec.a2021146317;

public class Calculator {
    int oldNumber, newNumber;

    public Calculator(int oldNumber, int newNumber) {
        this.oldNumber = oldNumber;
        this.newNumber = newNumber;
    }

    public void conta(char operacao){
        switch (operacao){
            case '+':
                System.out.println(soma());
                break;
            case '-':
                System.out.println(subtracao());
                break;
            case '*':
                System.out.println(multiplicacao());
                break;
            case '/':
                System.out.println(divisao());
                break;
            default:
                break;
        }
    }



    public int soma(){
        return oldNumber + newNumber;
    }

    public int subtracao(){
        return oldNumber - newNumber;
    }

    public int multiplicacao(){
        return oldNumber * newNumber;
    }

    public int divisao(){
        return oldNumber / newNumber;
    }




}