package pt.isec.a2021146317;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CalculatorTest {

    Calculator calc = new Calculator(2, 1);

    @Test
    void soma() {
        assertEquals(3, calc.soma());
    }

    @Test
    void subtracao() {
        assertEquals(1, calc.subtracao());
    }

    @Test
    void multiplicacao() {
        assertEquals(2, calc.multiplicacao());
    }

    @Test
    void divisao() {
        assertEquals(2, calc.divisao());
    }
}